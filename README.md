## TodoSpring

**TodoSpring** est une application vous permettant de gérer vos tâches grâce à une interface ergonomique et colorée.
Le back-end de l'application a été développé en Java avec l'aide du framework Spring Boot. Le front-end quant à lui
 a été réalisé avec les langages du Web ainsi qu'AngularJS.

### Heroku 

L'application est disponible sur Heroku au lien suivant : [https://todospring-mayolridel.herokuapp.com/](https://todospring-mayolridel.herokuapp.com/).

### Auteurs

- **[Loïc Mayol](https://gitlab.com/DO3B)**,
- **[Marius Ridel](https://gitlab.com/Marius_RIDEL)**.