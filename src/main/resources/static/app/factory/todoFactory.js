todoSpring.factory('todoFactory', ['$http', function($http) {
    var factory = {};

    factory.getLoggedUser = function (callback) {
        $http({
            method: 'GET',
            url: '/api/loggedUser'
        }).then(function successCallback(response) {
            callback(response)
        }, function errorCallback(err) {
            console.log('Error: ' + err.data.error);
        });
    };

    factory.getTaskSet = function (callback) {
        $http({
            method: 'GET',
            url: '/api/taskSet'
        }).then(function successCallback(response) {
            callback(response)
        }, function errorCallback(err) {
            console.log('Error: ' + err.data.error);
        });
    };

    factory.addTask = function (task, callback) {
        $http({
            method: 'POST',
            url: '/api/taskSet',
            data: task
        }).then(function successCallback(response) {
            callback(response);
        }, function errorCallback(err) {
            console.log('Error: ' + err.data.error);
        });
    };

    factory.removeTask = function (id, callback) {
        $http({
            method: 'DELETE',
            url: '/api/taskSet/' + id
        }).then(function successCallback(response) {
            callback(response);
        }, function errorCallback(err) {
            console.log('Error: ' + err.data.error);
        });
    };

    factory.updateTask = function (task, callback) {
        $http({
            method: 'PUT',
            url: '/api/taskSet',
            data: task
        }).then(function successCallback(response) {
            callback(response);
        }, function errorCallback(err) {
            console.log('Error: ' + err.data.error);
        });
    };

    return factory;
}]);