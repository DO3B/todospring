todoSpring.controller('ConnectionCtrl', ['$scope', '$http', '$window', '$timeout', '$location', function ($scope, $http, $window, $timeout, $location) {
    $scope.verifLogin = function(){
        if($location.absUrl().includes("?error")){
            $(".display-error").html("<ul> <li> Identifiants incorrects </li> </ul>");
            $(".display-error").css("display", "block");      }
    };

    $scope.verifLogin();
    $scope.register = function (user) {
        if($('#confirmpassword').val() == $scope.user.password ) {
            $http({
                method: 'POST',
                url: '/api/addUser',
                data: user
            }).then(function successCallback(response) {
                if (response.status == 201) {
                    alert("Votre compte a bien été créé ! Vous allez être redirigé vers la page de connexion.");
                    $timeout(function () {
                        $window.location.href = '/connection.html';
                    }, 4000);
                }
            }, function errorCallback(err) {
                $(".display-error").html("<ul> <li> " + err.data.message + " </li> </ul>");
                $(".display-error").css("display", "block");
            });
        } else {
            $(".display-error").html("<ul> <li> Les mots de passe ne correspondent pas.</li> </ul>");
            $(".display-error").css("display", "block");
        }
    };
}]);