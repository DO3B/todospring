todoSpring.controller('TodoCtrl', ['$http', '$scope','todoFactory', function ($http, $scope, todoFactory) {
    $scope.taskName="";

    $scope.getLoggedUser = function () {
        todoFactory.getLoggedUser(function (response) {
            $scope.user = response.data;
        });
    };

    $scope.refreshSet = function () {
        todoFactory.getTaskSet(function (response) {
            $scope.tasks = response.data;
        });
    };


    $scope.addTask = function () {
        var task = {
            text: $scope.taskName
        };
        todoFactory.addTask(task, function (response) {
            $scope.refreshSet();
        });
        $scope.taskName="";
    };

    $scope.removeTask = function (id) {
        todoFactory.removeTask(id, function (response) {
            $scope.refreshSet();
        });
    };

    $scope.updateTask = function (task) {
        if (task.text != "") {
            todoFactory.updateTask(task, function (response) {
                $scope.refreshSet();
            });
        } else{
            $scope.refreshSet();
        }
    };

    $scope.deleteSelected = function (){
        var i;
        var task;

        var selected = document.getElementById("invalide").getElementsByClassName("selected");

        for (i = 0; i < selected.length; i++) {
            task = JSON.parse(selected[i].getAttribute("task"))
            $scope.removeTask(task.id);
        }

        selected = document.getElementById("valide").getElementsByClassName("selected");

        for (i = 0; i < selected.length; i++) {
            task = JSON.parse(selected[i].getAttribute("task"));
            $scope.removeTask(task.id);
        }
    };

    $scope.coche = function () {
        var selected = document.getElementById("invalide").getElementsByClassName("selected");

        for (var i = 0; i < selected.length; i++) {
            var task = JSON.parse(selected[i].getAttribute("task"))
            task.done = "true";
            $scope.updateTask(task);
        }
    };

    $scope.decoche = function () {
        var selected = document.getElementById("valide").getElementsByClassName("selected");

        for (var i = 0; i < selected.length; i++) {
            var task = JSON.parse(selected[i].getAttribute("task"))
            task.done = "false";
            $scope.updateTask(task);
        }
    };


    $scope.isDone = function(ta) {
        return (ta.done == true);
    };

    $scope.getLoggedUser();
    $scope.refreshSet();

}]);