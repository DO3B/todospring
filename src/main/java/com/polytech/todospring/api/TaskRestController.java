package com.polytech.todospring.api;

import com.polytech.todospring.business.TaskService;
import com.polytech.todospring.business.TodoService;
import com.polytech.todospring.business.Task;
import com.polytech.todospring.config.ResourcesPath;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
public class TaskRestController {

    private TaskService taskService;
    private TodoService todoService;

    public TaskRestController(TaskService taskService, TodoService todoService) {
        this.taskService = taskService;
        this.todoService = todoService;
    }

    @RequestMapping(value = ResourcesPath.TASK, method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<Task> tasks(Principal user) {
        return taskService.fetchAll(user.getName());
    }

    @RequestMapping(value = ResourcesPath.TASK, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Task post(@RequestBody Task task, Principal user) {
        todoService.share(user.getName(), task);
        return task;
    }

    @RequestMapping(value = ResourcesPath.TASK + "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public int delete(@PathVariable("id") int  id, Principal user) {
        todoService.delete(user.getName(), id);
        return id;
    }

    @RequestMapping(value = ResourcesPath.TASK, method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public Task update(@RequestBody Task task, Principal user) {
        todoService.update(user.getName(), task);
        return task;
    }
}
