package com.polytech.todospring.api;

import com.polytech.todospring.business.Authority;
import com.polytech.todospring.business.UserService;
import com.polytech.todospring.business.User;
import com.polytech.todospring.business.exceptions.UsernameAlreadyExistsException;
import com.polytech.todospring.config.ResourcesPath;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

@RestController
public class LoginRestController {
    private UserService userService;

    public LoginRestController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = ResourcesPath.URL_PREFIXE + "/loggedUser", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Optional<String> currentUserName(Principal principal) {
        return Optional.ofNullable(principal.getName());
    }

    @RequestMapping(value = ResourcesPath.REGISTER, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void addUser(@RequestBody User user) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(hashedPassword);
        user.setEnabled(true);
        user.setAuthoritySet(Collections.singleton(new Authority("user", user)));
        try {
            userService.addUser(user);
        } catch (UsernameAlreadyExistsException e){
            throw new ResponseStatusException(HttpStatus.I_AM_A_TEAPOT, e.getMessage(), e);
        }
    }


}