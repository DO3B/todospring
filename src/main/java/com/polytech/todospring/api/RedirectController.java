package com.polytech.todospring.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RedirectController {


        @GetMapping("/login")
        public String login() {
            return "/connection.html";
        }

        @GetMapping("/register")
        public String register() {
            return "/register.html";
        }
        @GetMapping("/login?error")
        public String loginError() {
            return "/connection.html";
        }

        @GetMapping("/todoList")
        public String todoList() {
            return "/tasks.html";
        }
}
