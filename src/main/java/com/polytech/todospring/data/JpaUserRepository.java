package com.polytech.todospring.data;

import com.polytech.todospring.business.User;
import com.polytech.todospring.business.exceptions.UsernameAlreadyExistsException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Transactional
public class JpaUserRepository implements UserRepository {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public User findUserByName(String username) {
        return entityManager.find(User.class, username);
    }
    @Override
    public void addUser(User user) throws UsernameAlreadyExistsException {
        User isInDatabase = findUserByName(user.getUsername());
        if(isInDatabase == null){
            entityManager.persist(user);
        } else {
            throw new UsernameAlreadyExistsException(user.getUsername());
        }
    }
}
