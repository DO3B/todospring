package com.polytech.todospring.data;

import com.polytech.todospring.business.User;
import com.polytech.todospring.business.exceptions.UsernameAlreadyExistsException;

public interface UserRepository {
    void addUser(User user) throws UsernameAlreadyExistsException;
    User findUserByName(String username);
}
