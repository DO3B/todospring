package com.polytech.todospring.data;


import com.polytech.todospring.business.Task;
import com.polytech.todospring.business.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class JpaTaskRepository implements TaskRepository {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<Task> findAll(String username) {
        User user = entityManager.find(User.class, username);
        return user.getTaskList();
    }

    @Override
    public void save(String username, Task task) {
        User user = entityManager.find(User.class, username);
        user.getTaskList().add(task);
    }

    @Override
    public void remove(String username, int id){
        User user = entityManager.find(User.class, username);
        Task task = entityManager.find(Task.class, id);
        user.getTaskList().remove(task);
    }

    @Override
    public void update(String username, Task task){
        Task updateTask = entityManager.find(Task.class, task.getId());
        updateTask.setDone(task.isDone());
        updateTask.setText(task.getText());
    }
}
