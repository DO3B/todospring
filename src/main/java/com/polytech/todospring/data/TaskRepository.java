package com.polytech.todospring.data;

import com.polytech.todospring.business.Task;

import java.util.List;

public interface TaskRepository {
    List<Task> findAll(String username);
    void save(String username, Task task);
    void remove(String username, int task);
    void update(String username, Task task);
}
