package com.polytech.todospring.config;

import com.polytech.todospring.business.*;
import com.polytech.todospring.business.impl.UserServiceImpl;
import com.polytech.todospring.business.impl.TaskServiceImpl;
import com.polytech.todospring.business.impl.TodoServiceImpl;
import com.polytech.todospring.data.JpaUserRepository;
import com.polytech.todospring.data.JpaTaskRepository;
import com.polytech.todospring.data.UserRepository;
import com.polytech.todospring.data.TaskRepository;
import org.mariadb.jdbc.MariaDbDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
public class AppConfig {

    @Bean
    TaskRepository taskRepository() {
        return new JpaTaskRepository();
    }

    @Bean
    UserRepository userRepository() {
        return  new JpaUserRepository();
    }

    @Bean
    DataSource datasource() {
        MariaDbDataSource dataSource = new MariaDbDataSource();
        try{
            dataSource.setUrl("jdbc:mariadb://mysql-mayol.alwaysdata.net:3306/mayol_todospring");
            dataSource.setUser("mayol_java");
            dataSource.setPassword("mdpsupersecret");
            dataSource.setDatabaseName("mayol_todospring");
        } catch (SQLException e){
            System.err.println(e.getMessage());
        }
        return dataSource;
    }

    @Bean
    UserService userService() {
        return new UserServiceImpl(userRepository());
    }

    @Bean
    TodoService todoService() {
        return new TodoServiceImpl(taskRepository());
    }

    @Bean
    TaskService taskService(){
        return new TaskServiceImpl(taskRepository());
    }
}
