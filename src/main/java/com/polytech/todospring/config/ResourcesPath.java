package com.polytech.todospring.config;

public class ResourcesPath {
    private static final String URL_SEPARATOR = "/";
    private static final String API_PREFIXE = "api";
    public static final String URL_PREFIXE = URL_SEPARATOR + API_PREFIXE + URL_SEPARATOR;
    public static final String TASK = URL_PREFIXE + "taskSet";
    public static final String REGISTER = URL_PREFIXE + "addUser";
    public static final String LOGIN = URL_SEPARATOR + "login";
    public static final String LOGOUT = URL_SEPARATOR + "logout";
}
