package com.polytech.todospring.business;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "tasks")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "text")
    private String text;

    @CreationTimestamp
    @Column(name = "date")
    private Timestamp date;

    @Column(name = "done")
    private boolean done;

    @ManyToOne
    private User user;

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", date=" + date +
                ", done=" + done +
                ", user=" + user +
                '}';
    }

    public Task(String text, boolean done) {
        this.text = text;
        this.done = done;
    }

    public Task(String text, Timestamp date) {
        this.text = text;
        this.date = date;
    }

    public Task(String text, Timestamp date, boolean done) {
        this.text = text;
        this.date = date;
        this.done = done;
    }

    public Task(){

    }

    public Task(int id, String text, Timestamp date, boolean done) {
        this.id = id;
        this.text = text;
        this.date = date;
        this.done = done;
    }

    public Task(String text) {
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
