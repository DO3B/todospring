package com.polytech.todospring.business;

import java.util.List;

public interface TaskService {
    List<Task> fetchAll(String username);
}