package com.polytech.todospring.business;

import com.polytech.todospring.business.exceptions.UsernameAlreadyExistsException;

public interface UserService {
    void addUser(User user) throws UsernameAlreadyExistsException;
    User findUserByName(String username);
}
