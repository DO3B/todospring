package com.polytech.todospring.business;

public interface TodoService {
    void share(String username, Task task);
    void delete(String username, int id);
    void update(String username, Task task);
}
