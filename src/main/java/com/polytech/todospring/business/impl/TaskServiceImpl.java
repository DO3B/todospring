package com.polytech.todospring.business.impl;

import com.polytech.todospring.business.Task;
import com.polytech.todospring.business.TaskService;
import com.polytech.todospring.data.TaskRepository;

import java.util.List;

public class TaskServiceImpl implements TaskService {

    private TaskRepository taskRepository;

    public TaskServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<Task> fetchAll(String username) {
        return taskRepository.findAll(username);
    }
}
