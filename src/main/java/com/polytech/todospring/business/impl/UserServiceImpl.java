package com.polytech.todospring.business.impl;


import com.polytech.todospring.business.UserService;
import com.polytech.todospring.business.User;
import com.polytech.todospring.business.exceptions.UsernameAlreadyExistsException;
import com.polytech.todospring.data.UserRepository;

public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void addUser(User user) throws UsernameAlreadyExistsException {
        userRepository.addUser(user) ;
    }

    public User findUserByName(String username){
        return userRepository.findUserByName(username);
    }
}
