package com.polytech.todospring.business.impl;

import com.polytech.todospring.business.Task;
import com.polytech.todospring.business.TodoService;
import com.polytech.todospring.data.TaskRepository;

public class TodoServiceImpl implements TodoService {

    private TaskRepository taskRepository;

    public TodoServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void share(String username, Task task) {
        taskRepository.save(username, task);
    }

    @Override
    public void delete(String username, int id) {
        taskRepository.remove(username, id);
    }

    @Override
    public void update(String username, Task task){
        taskRepository.update(username, task);
    }
}

