package com.polytech.todospring.business.exceptions;

public class UsernameAlreadyExistsException extends  Exception {

    public UsernameAlreadyExistsException(String username) {
        super("Le nom d'utilisateur " +  username + " est déjà utilisé !");
    }
}
