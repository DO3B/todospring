-- tasks.sql
INSERT INTO users (username, password, enabled) VALUES ('Archibald', '$2a$10$0vM5hufkzW3hans7uhI6wuP80mukWmkceaJJMbpUy6O6ewi9igCwO',1);

INSERT INTO tasks (text, date, done, user_username) VALUES ('Il était une fois', now(), 0, 'Archibald');
INSERT INTO tasks (text, date, done, user_username) VALUES ('une envie de mourir', now(), 0, 'Archibald');
INSERT INTO tasks (text, date, done, user_username) VALUES ('mais maintenant ça va mieux', now(), 0, 'Archibald');
INSERT INTO tasks (text, date, done, user_username) VALUES ('Jpp aled', now(), 0, 'Archibald');
