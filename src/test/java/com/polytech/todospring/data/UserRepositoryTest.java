package com.polytech.todospring.data;

import com.polytech.todospring.business.User;
import com.polytech.todospring.business.UserService;
import com.polytech.todospring.business.exceptions.UsernameAlreadyExistsException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import javax.transaction.Transactional;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureDataJpa
@Transactional
public class UserRepositoryTest {
    @Autowired
    private UserService userService;

    @Test
    public void should_create_user_and_find_user_by_name() throws UsernameAlreadyExistsException {
        User register = new User("Patrick","mdpsupersecret");
        userService.addUser(register);
        User user = userService.findUserByName("Patrick");
        Assert.assertNotNull(user);
        Assert.assertEquals(register,user);
    }

    @Test
    @Sql(scripts={"classpath:dbunit/users.sql"})
    public void should_find_user() {
        User user = userService.findUserByName("Archibald");
        Assert.assertNotNull(user);
    }
}

