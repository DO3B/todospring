package com.polytech.todospring.data;

import com.polytech.todospring.business.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.List;

import static java.util.Arrays.asList;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureDataJpa
@Transactional
public class TaskRepositoryTest {

    @Autowired
    private TodoService todoService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private UserService userService;

    @Test
    @Sql(scripts={"classpath:dbunit/tasks.sql"})
    public void should_fetch_all_tasks_of_a_user(){
        List<Task> taskList = taskService.fetchAll("Archibald");
        Assert.assertEquals(4,taskList.size());
    }

    @Test
    @Sql(scripts={"classpath:dbunit/tasks.sql"})
    public void should_add_tasks_to_user_and_fetch_them(){
        Task task1 = new Task("Trouver une façon d'écrire des tests en BD");
        Task task2 = new Task("Se tirer une balle");
        todoService.share("Archibald", task1);
        todoService.share("Archibald", task2);


        List<Task> taskList = taskService.fetchAll("Archibald");
        Assert.assertEquals(6,taskList.size());
        Assert.assertTrue(taskList.containsAll(asList(task1,task2)));
    }

    @Test
    @Sql(scripts={"classpath:dbunit/tasks.sql"})
    public void should_delete_task() {
        User user = userService.findUserByName("Archibald");
        List<Task> taskList = user.getTaskList();
        Task task = taskList.get(2);

        todoService.delete(user.getUsername(), task.getId());
        Assert.assertEquals(3,taskList.size());
    }

    @Test
    @Sql(scripts={"classpath:dbunit/tasks.sql"})
    public void should_update_task() {
        User user = userService.findUserByName("Archibald");
        List<Task> taskList = user.getTaskList();
        Task task = taskList.get(2);

        task.setDone(true);
        task.setText("Une modification qui se passe plutôt bien !");
        todoService.update(user.getUsername(), task);
        List<Task> taskList2 = taskService.fetchAll("Archibald");

        Assert.assertEquals(taskList.get(2),taskList2.get(2));
        Assert.assertTrue(taskList2.get(2).isDone());
    }
}